#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
# check_file_exists.py - A check plugin for monitoring if file exists.
# Copyright (C) 2019 
#
# Version: 0.1.0
#
# ------------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
# ------------------------------------------------------------------------------

from __future__ import print_function
import sys
import os

try:
    from enum import Enum
    import argparse
    import datetime

except ImportError as e:
    print("Missing python module: {0}".format(e.msg))
    sys.exit(255)


class NagiosState(Enum):
    OK = 0
    WARNING = 1
    CRITICAL = 2
    UNKNOWN = 3


class CheckFile:

    options = {}
    perfdata = ""
    checkResult = -1
    checkMessage = ""

    def getPerfdata(self):
      return self.perfdata

    def checkOutput(self):
        message = self.checkMessage
        if self.perfdata:
            message += self.getPerfdata()

        self.output(self.checkResult, message)

    def output(self, returnCode, message):
        prefix = returnCode.name

        message = '{} - {}'.format(prefix, message)

        print(message)
        sys.exit(returnCode.value)


    def check(self):

        self.checkResult = NagiosState.OK
        today     = datetime.datetime.now()
        
        file_name  = "{0}\\{1}-robocopy.zip".format(self.options.file_path, today.strftime("%d-%m-%y"))

        self.checkMessage += "File {0} exists".format(file_name)

        if not os.path.exists(file_name):
            self.checkResult = NagiosState.CRITICAL
            self.checkMessage = "File {0} not exists".format(file_name)

        self.output(self.checkResult, self.checkMessage)
        self.checkOutput()

    def parseOptions(self):
        p = argparse.ArgumentParser(description='Check if file exists')

        opts = p.add_argument_group('Options')

        opts.add_argument("-p", "--path", dest='file_path', required=True, help="Local file path")

        opts.add_argument('-w', '--warning', dest='treshold_warning', type=int,
                                help='Warning treshold for check value')
        opts.add_argument('-c', '--critical', dest='treshold_critical', type=int,
                                help='Critical treshold for check value')

        opts = p.parse_args()

        self.options = opts


    def __init__(self):
        self.parseOptions()


filePresence = CheckFile()
filePresence.check()